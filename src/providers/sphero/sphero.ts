import { Injectable } from '@angular/core';
import { BLE } from '@ionic-native/ble';

/*
  Generated class for the SpheroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SpheroProvider {

  constructor( private ble: BLE ) {
    console.log('Hello SpheroProvider Provider');

  }
}
